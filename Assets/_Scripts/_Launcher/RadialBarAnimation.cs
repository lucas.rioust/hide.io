﻿#region Using...

using UnityEngine;

#endregion

public class RadialBarAnimation : MonoBehaviour
{
    #region Parameters definition

    [SerializeField] RectTransform rectTransform;
    [SerializeField] float rotationSpeed;

    #endregion

    #region Monobehaviour methods

    void Update()
    {
        rectTransform.Rotate(rectTransform.forward, Time.deltaTime*rotationSpeed);
    }

    #endregion
}
