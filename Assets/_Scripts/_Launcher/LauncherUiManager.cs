﻿#region Using ...

using UnityEngine;
using System.Collections.Generic;

using Photon.Realtime;
using Photon.Pun;

using TMPro;

#endregion

public class LauncherUiManager : MonoBehaviourPunCallbacks
{
    #region Parameters definition

    [Tooltip("The menu's radial bar must be linked")]
    [SerializeField] GameObject RadialBar;

    [Tooltip("The menu's server chosing menu must be linked")]
    [SerializeField] GameObject ChosingMenu;

    [Tooltip("The launcher must be linked")]
    [SerializeField] Launcher launcher;

    [Tooltip("The menu's dropdown must be linked")]
    [SerializeField] private TMP_Dropdown dropdown;

    [Tooltip("The menu's name field must be linked")]
    [SerializeField] private TMP_InputField nameField;

    [Tooltip("The menu's warning message must be linked")]
    [SerializeField] private GameObject warningMessage;

    // The current server dropdown choice.
    private string currentDropdownSelection = "New room";
    
    // The current player name choice.
    public string currentName = "";

    // The current list of room to display in the menu's dropdown.
    private List<RoomInfo> rooms = new List<RoomInfo>();

    #endregion

    #region Monobehaviour methods

    public void Start()
    {
        if (!PhotonNetwork.InLobby)
            HideRoomSelectionMenu();
    }

    #endregion

    #region Private Controller Methods

    // Hides the Radial Bar and show the room selection menu.
    private void DisplayRoomSelectionMenu()
    {
        ChosingMenu.SetActive(true);
        RadialBar.SetActive(false);
        nameField.ActivateInputField();
    }

    // Shows the Radial Bar and hides the room selection menu.
    private void HideRoomSelectionMenu()
    {
        RadialBar.SetActive(true);
        ChosingMenu.SetActive(false);
    }

    // Updates the different options the room selection menu's dropdown.
    private void UpdateRoomList()
    {
        dropdown.ClearOptions();
        dropdown.AddOptions(new List<string> { "New room" });
        foreach(RoomInfo room in rooms)
            dropdown.AddOptions(new List<string> { room.Name + "  (" + room.PlayerCount + "/" + room.MaxPlayers + ")" });
    }
    
    #endregion

    #region Public UI Input events methods

    //Update the currentDropdownSelction parameter whenever it changes.
    public void UpdateRoomSelection()
    {
        string choice = dropdown.options[dropdown.value].text;

        //No need to crop the string if the player wants to create a new room.
        if (choice == "New room")
            return;
        
        //Crop the string to keep only the name of the room.
        int nameEndIndex = choice.IndexOf("  (");
        currentDropdownSelection = choice.Substring(0, nameEndIndex);
    }

    //Update the nameField parameter whenever it changes.
    public void UpdateNameSelection()
    {
        currentName = nameField.text;
    }

    //When the player clicks the connection button, asks the launcher class to join the chosen room or create a new one.
    public void Connect()
    {
        //First check if the nickname is valid
        if (currentName == "")
        {
            warningMessage.SetActive(true);
            return;
        }

        //Loading Wheel
        HideRoomSelectionMenu();

        //Sets the nickname of the client before joining a room.
        PhotonNetwork.NickName = currentName;

        //If the choice is "New room", asks the launcher class to create and join a new room.
        if (currentDropdownSelection == "New room")
            launcher.ConnectToNew();

        //Else, ask to join the selected room.
        else
            launcher.ConnectToServer(currentDropdownSelection);
        
    }

    #endregion

    #region PUN Callbacks override

    //Called whenever a room is created or deleted. Warning : roomList actually only contains the changes, that's why we use another list "rooms" to cache everything.
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        //Update the "rooms" list
        foreach (RoomInfo roomInfo in roomList)
        {
            if (roomInfo.RemovedFromList)
                rooms.Remove(roomInfo);
            else if (!rooms.Contains(roomInfo))
                rooms.Add(roomInfo);
        }

        //Update the UI
        UpdateRoomList();
    }

    //Whenever the client is connected to a lobby, hide the loading bar and display the room selection menu.
    public override void OnJoinedLobby()
    {
        DisplayRoomSelectionMenu();
    }

    #endregion
}
