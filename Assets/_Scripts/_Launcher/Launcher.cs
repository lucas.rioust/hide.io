﻿#region Using ...

using UnityEngine;

using Photon.Realtime;
using Photon.Pun;

#endregion

public class Launcher : MonoBehaviourPunCallbacks
{
    #region Parameters definition

    [Tooltip("The current version of the game")]
    [SerializeField] string gameVersion = "1";

    #endregion

    #region Monobehaviour methods
    void Awake()
    {
        // Allows the client to load the correct scene when joining an existing lobby.
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Start()
    {
        // Connects to the photon online server if the player is disconnected. 
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "eu";

            PhotonNetwork.ConnectUsingSettings();
            PhotonNetwork.GameVersion = gameVersion;
        }
    }

    #endregion

    #region Launcher methods

    /// <summary>
    /// Creates a new server and join it.
    /// </summary>
    public void ConnectToNew()
    {
        // At this point, we should be connected to the server, but we test it just in case.
        if (PhotonNetwork.IsConnected)
        {
            // The default roomOptions
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.MaxPlayers = 10;
            
            // Create a new default room and connect the player to it
            PhotonNetwork.JoinOrCreateRoom(Random.Range(0,10000).ToString(), roomOptions, TypedLobby.Default);
        }
    }

    /// <summary>
    /// Joins an existing server, named serverName.
    /// </summary>
    /// <param name="serverName">At this point, serverName should be an existing room name</param>
    public void ConnectToServer(string serverName)
    {
        // At this point, we should be connected to the server, but we test it just in case.
        if (PhotonNetwork.IsConnected)
        {
            // Joins the room with the name 'serverName'
            PhotonNetwork.JoinRoom(serverName);
        }
    }
    #endregion

    #region PUN Callbacks

    public override void OnConnectedToMaster()
    {
        Debug.Log("Launcher : OnConnectedToMaster() was called by PUN");
        // Once the client is connected to the Master Server, he'll join the default Lobby.
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.NickName + " is now in the room.");
        Debug.Log("Room's name : " + PhotonNetwork.CurrentRoom.Name);

        // We only load if we are the first player, else we rely on `PhotonNetwork.AutomaticallySyncScene` to sync our instance scene.
        if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        {
            PhotonNetwork.LoadLevel("Playroom");
        }
    }
    #endregion
}

