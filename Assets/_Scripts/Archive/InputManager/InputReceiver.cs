﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class InputReceiver : MonoBehaviourPunCallbacks
{
    public InputControllable inputControllable;
    public bool getInput;

    public void Start()
    {
        if (!photonView.IsMine) return;
        StartCoroutine(InputLoop(.01f));
    }

    public IEnumerator InputLoop(float deltaTime)
    {
        
        getInput = true;
        InputData input;
        while(getInput)
        {
            input = GetInput();
            photonView.RPC("SendData", RpcTarget.AllViaServer, input.toObjectArray());
            yield return new WaitForSeconds(deltaTime);
        }
    }

    public InputData GetInput()
    {
        var result = new InputData();
        result.movementVector = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        result.mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        return result;
    }

    [PunRPC]
    public void SendData(object[] param)
    {
        inputControllable.SetInput(InputData.fromObjectArray(param));
    }
}

[Serializable]
public class InputData
{
    public Vector2 movementVector;
    public Vector2 mousePosition; //doit être en coordonnée WORLD ou SELF, mais pas Camera
    public bool actionTir;

    public InputData()
    {
        mousePosition = Vector2.zero;
        movementVector = Vector2.zero;
        actionTir = false;
    }

    public InputData(Vector2 movementVector,Vector2 mousePosition,bool actionTir)
    {
        
        this.movementVector = movementVector;
        this.mousePosition = mousePosition;
        this.actionTir = actionTir;
    }

    public object[] toObjectArray()
    {
        return new object[] { movementVector, mousePosition, actionTir };
    }

    public static InputData fromObjectArray(object[] param)
    {
        return new InputData((Vector2)param[0], (Vector2)param[1], (bool)param[2]);
    }

    public static bool operator! (InputData inputData)
    {
        return inputData == null;
    }
}
