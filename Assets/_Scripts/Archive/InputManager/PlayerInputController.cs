﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerInputController : InputControllable
{
    [SerializeField] private float speed;
    [SerializeField] private Transform rotationAvatar;
    [SerializeField] private GameObject playerLight;
    [SerializeField] private Rigidbody2D rigi;
    [SerializeField] private float acceleration = 5f;
    [SerializeField] private float deceleration = 3f;

    private void Start()
    {
        PhotonNetwork.SendRate = 100;
        PhotonNetwork.SerializationRate = 100;
        if (photonView.IsMine == false && PhotonNetwork.IsConnected == true)
        {
            playerLight.SetActive(false);
            return;
        }
    }

    protected override void ProcessInput(InputData inputData)
    {
        rotationAvatar.right = new Vector2(inputData.mousePosition.x - transform.position.x, inputData.mousePosition.y - transform.position.y);

        //Deplacer dans FixedUpdate ?
        var targetSpeed = Vector2.zero;
        var coef = deceleration;
        if (inputData.movementVector.magnitude > .05)
        {
            targetSpeed = inputData.movementVector / inputData.movementVector.magnitude * speed;
            coef = acceleration;
        }
        rigi.velocity = Vector3.Lerp(rigi.velocity, targetSpeed, coef / 20);
    }
}
