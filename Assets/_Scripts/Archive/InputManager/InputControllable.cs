﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public abstract class InputControllable : MonoBehaviourPunCallbacks
{
    protected InputData previousInputData;
    public void SetInput(InputData inputData)
    {
        if (!previousInputData) previousInputData = new InputData();
        ProcessInput(inputData);
    }

    protected abstract void ProcessInput(InputData inputData);

}
