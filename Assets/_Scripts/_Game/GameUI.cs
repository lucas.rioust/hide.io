﻿#region Using...

using Photon.Pun;
using UnityEngine;

#endregion

public class GameUI : MonoBehaviourPun
{
    #region Parameters definition

    [Tooltip("An array with the 10 name slots of the player list menu.")]
    [SerializeField] private TMPro.TMP_Text[] playerStatus = new TMPro.TMP_Text[10];

    [Tooltip("The ping indicator")]
    [SerializeField] private TMPro.TMP_Text pingIndicator;

    [Tooltip("The gameObject containing the player list UI")]
    [SerializeField] private GameObject playerList;

    [Tooltip("The death message")]
    [SerializeField] private TMPro.TMP_Text deathMessage;

    [Tooltip("The gameObject containing the death UI")]
    [SerializeField] private GameObject deathUI;

    #endregion

    #region Monobehaviour methods

    private void Start()
    {
        HideDeathUI();
        HidePlayerList();
        UpdatePlayerList();
    }

    private void Update()
    {
        UpdatePing();
    }

    #endregion

    #region View Methods

    public void ShowPlayerList()
    {
        playerList.SetActive(true);
        for (int i = 0; i < PhotonNetwork.PlayerList.Length; i++)
        {
            Debug.Log(PhotonNetwork.PlayerList[i].NickName);
        }
    }

    public void HidePlayerList()
    {
        playerList.SetActive(false);
    }

    public void ShowDeathUI(string KillerName)
    {
        deathMessage.text = KillerName + " just bamboozled you !";
        deathUI.SetActive(true);
    }

    public void HideDeathUI()
    {
        deathUI.SetActive(false);
    }

    private void UpdatePing()
    {
        int ping = PhotonNetwork.GetPing();
        pingIndicator.text = ping + " ms";
        if (ping < 50)
            pingIndicator.color = Color.green;
        else if (ping < 100)
            pingIndicator.color = Color.yellow;
        else
            pingIndicator.color = Color.red;
    }
    #endregion

    #region Model methods

    public void UpdatePlayerList()
    {
        int playerListLength = PhotonNetwork.PlayerList.Length;

        for (int i = 0; i < playerListLength; i++)
        {
            playerStatus[i].text = PhotonNetwork.PlayerList[i].NickName;
        }

        for (int i = playerListLength; i < 10; i++)
        {
            playerStatus[i].text = "";
        }
    }

    #endregion
}
