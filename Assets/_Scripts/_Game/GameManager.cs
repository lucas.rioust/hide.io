﻿#region Using ...

using UnityEngine;
using UnityEngine.SceneManagement;

using Photon.Pun;
using Photon.Realtime;
using System.Linq;
using System.Collections;

#endregion

public class GameManager : MonoBehaviourPunCallbacks
{
    #region Parameters definition

    public static GameManager Instance;

    [Tooltip("The prefab to use for representing the player. It needs to be in the resources folder")]
    public GameObject playerPrefab;

    [Tooltip("The 10 Spawning points in the map")]
    public GameObject[] spawnPoints = new GameObject[10];

    [Tooltip("The gameObject containing the UI")]
    public GameUI gameUI;

    private int spawnPointsCount;

    #endregion

    #region Monobehaviour methods

    void Start()
    {
        Instance = this;

        // Once the scene is loeaded, spawn a character for the local player. Tt gets synced by using PhotonNetwork.Instantiate
        PhotonNetwork.Instantiate(this.playerPrefab.name, new Vector3(0f,0f,0f), Quaternion.identity, 0);
    }

    #endregion

    #region GameManager public methods

    public object[] SpawnPosition()
    {
        System.Random rnd = new System.Random();
        spawnPoints = spawnPoints.OrderBy(x => rnd.Next()).ToArray();

        object[] p = new object[spawnPoints.Length];
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            p[i] = (object)spawnPoints[i].transform.position;
        }
        return p;
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();
    }

    #endregion

    #region PUN Callbacks overrides

    public override void OnLeftRoom()
    {
        //When the room is left, we must load the lobby
        SceneManager.LoadScene(0);
    }
    
    public override void OnPlayerEnteredRoom(Player other)
    {
        Debug.LogFormat(other.NickName + " just joined the room."); // Everytime another player joins the room.
        gameUI.UpdatePlayerList();
    }


    public override void OnPlayerLeftRoom(Player other)
    {
        Debug.LogFormat(other.NickName + " just left the room."); // Everytime another player exits the room.
        gameUI.UpdatePlayerList();
    }

    #endregion
}