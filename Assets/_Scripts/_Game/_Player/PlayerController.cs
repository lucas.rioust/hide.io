﻿#region Using...

using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

#endregion

public class PlayerController : MonoBehaviourPunCallbacks
{
    #region Parameters definition

    public static Dictionary<int, PlayerController> dicPlayerController = new Dictionary<int, PlayerController>();

    CameraController cameraController;

    [SerializeField] private GunController gunController;

    [SerializeField] private float gunCooldown = .5f;

    private float stopWatchGunCooldown = 0;

    [SerializeField] private float gunStartLockTime = 2f;

    private float stopWatchGunLockTime = 0f;

    [SerializeField] private float speed;

    [SerializeField] private Transform rotationAvatar;

    [SerializeField] private GameObject playerLight;

    [SerializeField] private Rigidbody2D rigi;

    [SerializeField] private float acceleration = 20f;

    [SerializeField] private float deceleration = 1f;

    private Vector2 _direction;

    private Vector2 _mousePos;

    private bool isDead;

    public static int deadPlayers = 0;

    #endregion

    #region MonoBehaviour methods

    private void Awake()
    {
        if (dicPlayerController.ContainsKey(photonView.ViewID))
            dicPlayerController.Remove(photonView.ViewID);
        dicPlayerController.Add(photonView.ViewID, this);
    }

    private void Start()
    {
        PhotonNetwork.SendRate = 100;
        PhotonNetwork.SerializationRate = 100;
        stopWatchGunLockTime = Time.time;
        playerLight.SetActive(photonView.IsMine);

        cameraController = this.gameObject.GetComponent<CameraController>();

        if (photonView.IsMine)
        {
            foreach (var id in dicPlayerController.Keys)
            {
                Kill(id);
                break;
            }
            cameraController.OnStartFollowing();
        }
    }
    
    private void Update()
    {
        if (photonView.IsMine && !isDead)
        {
            GetMouseAndKeyboard();
        }
    }

    
    private void FixedUpdate()
    {
        if(photonView.IsMine && !isDead)
        {
            #region Velocity calculation

            var targetSpeed = Vector2.zero;
            var coef = deceleration;
            if (_direction.magnitude > .05)
            {
                targetSpeed = _direction / _direction.magnitude * speed;
                coef = acceleration;
            }
            rigi.velocity = Vector3.Lerp(rigi.velocity, targetSpeed, coef / 20);

            #endregion

            #region Rotation calculation

            rotationAvatar.right = new Vector2(_mousePos.x - transform.position.x, _mousePos.y - transform.position.y);

            #endregion
        }
    }

    private void OnApplicationQuit()
    {
        photonView.RPC(nameof(OnLeaveRoom), RpcTarget.All);
        CheckEndGame();
    }

    #endregion

    #region Player inputs

    private void GetMouseAndKeyboard()
    {
        #region Horizontal and Vertical movement

        _direction = new Vector2();
        _direction.x = Input.GetAxis("Horizontal");
        _direction.y = Input.GetAxis("Vertical");

        #endregion

        #region Mouse position

        _mousePos = Input.mousePosition;
        _mousePos = Camera.main.ScreenToWorldPoint(_mousePos);

        #endregion

        #region Mouse click

        if ((Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))&& Time.time - stopWatchGunCooldown > gunCooldown && Time.time - stopWatchGunLockTime > gunStartLockTime)
        {
            gunController.StartToShot();
            stopWatchGunCooldown = Time.time;
        }


        #endregion

        #region Tabulation

        if (Input.GetKeyDown(KeyCode.Tab))
            GameManager.Instance.gameUI.ShowPlayerList();

        else if (Input.GetKeyUp(KeyCode.Tab))
            GameManager.Instance.gameUI.HidePlayerList();


        #endregion

        #region Escape Key

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            photonView.RPC(nameof(OnLeaveRoom), RpcTarget.All);
            CheckEndGame();
            PhotonNetwork.LeaveRoom();
        }

        #endregion
    }
    #endregion

    #region PlayerController methods

    public void Kill(int killer_ID){
        transform.position = Vector3.one * 100;
        isDead = true;
        stopWatchGunLockTime = Time.time;
        if (photonView.IsMine)
        {
            cameraController.SetTarget(dicPlayerController[killer_ID].transform);
            dicPlayerController[killer_ID].playerLight.SetActive(true);
            
            GameManager.Instance.gameUI.ShowDeathUI(dicPlayerController[killer_ID].photonView.Owner.NickName);
            CheckEndGame();
        }
        else foreach(var player in dicPlayerController.Values)
        {
                if (player.photonView.IsMine && player.cameraController.GetTarget() == transform)
                {
                    player.cameraController.SetTarget(dicPlayerController[killer_ID].transform);
                    dicPlayerController[killer_ID].playerLight.SetActive(true);
                }
        }
    }

    public void CheckEndGame()
    {
        int deadCount = 0;
        foreach(var player in dicPlayerController.Values)
        {
            if (player.isDead) deadCount++;
        }

        if (deadCount >= dicPlayerController.Count-1)
        {
            photonView.RPC(nameof(EndGame), RpcTarget.All);
        }
    }

    public void ResetOnEndGame()
    {
        isDead = false;
        if (photonView.IsMine)
        {
            Camera.main.transform.position = photonView.transform.position + Vector3.forward * -10; GameManager.Instance.gameUI.HideDeathUI();
            cameraController.transform.position = photonView.transform.position;
            cameraController.SetTarget(photonView.transform);
            stopWatchGunLockTime = Time.time;
            playerLight.SetActive(true);
        }
        else
        {
            playerLight.SetActive(false);
        }
    }

    private void RespawnEveryone(params object[] spawnList)
    {
        int i = 0;
        object[] parameters = new object[1];
        foreach (var player in dicPlayerController.Values)
        {
            parameters[0] = spawnList[i++];
            photonView.RPC(nameof(Respawn), player.photonView.Owner, parameters as object);
        }
    }

    #endregion

    #region PunRPC

    [PunRPC]
    public void EndGame()
    {
        foreach(var player in dicPlayerController.Values)
        {
            player.ResetOnEndGame();
            player.RespawnEveryone(GameManager.Instance.SpawnPosition());
        }
    }

    [PunRPC]
    private void Respawn(params object[] respawnPoint)
    {
        photonView.transform.position = (Vector3) respawnPoint[0];
    }
    
    [PunRPC]
    public void OnLeaveRoom()
    {
        dicPlayerController.Remove(photonView.ViewID);
        if (isDead) deadPlayers--;
    }
    #endregion
}

