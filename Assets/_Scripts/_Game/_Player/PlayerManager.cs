﻿#region Using...

using Photon.Pun;

#endregion

public class PlayerManager :  MonoBehaviourPunCallbacks
{
    #region Monobehaviour methods
    void Start()
    {
        CameraController cameraController = this.gameObject.GetComponent<CameraController>();
        
        if (photonView.IsMine)
                cameraController.OnStartFollowing();
    }
    #endregion
}
