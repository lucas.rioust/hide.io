﻿#region Using...

using UnityEngine;

#endregion

public class CameraController : MonoBehaviour
{
    #region Parameters definition

    [SerializeField] private float cameraSmoothSpeed = 1f;

    private Transform cameraTransform;

    private Transform target;

    private Vector3 offset;

    private bool isFollowing;

    #endregion

    #region Monobehaviour methods

    private void FixedUpdate()
    {
        if(isFollowing)
            cameraTransform.position = Vector3.Lerp(cameraTransform.position,target.position + offset,cameraSmoothSpeed/20);
    }

    #endregion

    #region Public CameraController methods

    public void OnStartFollowing()
    {
        cameraTransform = Camera.main.transform;
        offset = cameraTransform.position - transform.position;
        target = transform;
        isFollowing = true;
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    public Transform GetTarget()
    {
        return target;
    }

    #endregion
}
