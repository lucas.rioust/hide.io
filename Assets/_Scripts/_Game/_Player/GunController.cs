﻿#region Using...

using System.Collections;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

#endregion

public class GunController : MonoBehaviourPunCallbacks
{
    #region Parameters definition

    [SerializeField] private PhotonView parentPhotonView;

    [SerializeField] private Transform gun;

    [SerializeField] private Light2D shootingLight;

    [SerializeField] private float shootingLightMaxIntensity;

    [SerializeField] private GameObject bullet;

    [SerializeField] private Animator animator;

    private float currentAimingDistance;
    private Vector3 currentAimingPoint;
    private GameObject currentPlayerAimedAt;

    #endregion

    #region Monobehaviour methods

    private void Start()
    {
        if(shootingLight)
            shootingLight.intensity = 0;
    }

    void Update()
    {
        if (parentPhotonView.IsMine == false && PhotonNetwork.IsConnected == true)
            return;

        //Check what player is currently aiming at
        CheckCurrentAim();
    }

    #endregion

    #region Private GunController methods

    //Computes the laser position, and check if an ennemy is being aimed
    private void CheckCurrentAim()
    {
        currentPlayerAimedAt = null;

        RaycastHit2D hit;
        hit = Physics2D.Raycast(gun.position, gun.right);

        if (hit && hit.collider.tag == "Player")
            currentPlayerAimedAt = hit.transform.gameObject;

        currentAimingDistance = hit.distance;
        currentAimingPoint = hit.point;
    }

    //The muzzle flash animation when the shot is fired
    IEnumerator lightAnimation(float delay)
    {
        for(int i = 0; i < 10; i++)
        {
            shootingLight.intensity += shootingLightMaxIntensity / 10;
            yield return new WaitForSecondsRealtime(delay *Time.deltaTime/ 20);
        }
        for (int i = 0; i < 10; i++)
        {
            shootingLight.intensity -= shootingLightMaxIntensity / 10;
            yield return new WaitForSecondsRealtime(delay * Time.deltaTime / 20);
        }
    }

    #endregion

    #region Public GunController methods

    public void StartToShot()
    {
        //If a player is aimed at, he will be passed in the Shoot function and get shot. Else, the shot will occur but no one will be hurt.
        var i = -1;
        CheckCurrentAim();
        if (currentPlayerAimedAt)
            i = currentPlayerAimedAt.GetComponent<PlayerController>().photonView.ViewID;

        photonView.RPC(nameof(Shoot), RpcTarget.AllViaServer,i,photonView.ViewID);
    }

    #endregion

    #region PunRPC public methods

    //Called when THE player did shot. Sends the information to the other clients. If id != -1, informs the player with the 
    //corresponding ID that he's been shot.
    [PunRPC]
    public void Shoot(int id,int killer_ID)
    {
        //Debug.Log(currentAimingDistance);
        StartCoroutine(lightAnimation(.05f));
        var b = Instantiate(bullet, gun.position, Quaternion.identity);
        b.transform.localScale = Vector3.one * .2f;
        b.transform.right = gun.right;
        var bul = b.GetComponent<Bullet>();
        bul.endPoint = currentAimingPoint;
        animator.SetTrigger("Tir");
        Destroy(b, currentAimingDistance / bul.speed);
        if (id != -1)
        {
            PlayerController.dicPlayerController[id].Kill(killer_ID);
        }
    }

    #endregion
}
