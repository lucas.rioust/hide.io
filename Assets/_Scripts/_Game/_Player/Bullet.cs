﻿#region Using...

using UnityEngine;

#endregion

public class Bullet : MonoBehaviour
{
    #region Parameters definition
    public float speed;
    public GameObject trail;
    public Vector3 endPoint;
    public ParticleSystem sparkle;
    #endregion

    #region Monobehaviour methods
    private void FixedUpdate()
    {
        transform.Translate(this.transform.right * speed * Time.fixedDeltaTime,Space.World);
    }

    private void OnDestroy()
    {
        endPoint.z = transform.position.z;
        trail.transform.SetParent(null);
        sparkle.transform.SetParent(null);
        sparkle.Play();
        sparkle.transform.position = endPoint;
        trail.transform.position = endPoint;
        Destroy(sparkle, 2f);
        
    }
    #endregion
}
